package main

import "fmt"

func main() {
	const baseCuadrado = 10
	areaCuadrado := baseCuadrado * baseCuadrado
	fmt.Println("Area cuadrado:", areaCuadrado)

	x := 10
	y := 50

	//Suma
	result := x + y
	fmt.Println("Suma:", result)

	//Resta
	result = x - y
	fmt.Println("Resta:", result)

	//Multiplicación
	result = x * y
	fmt.Println("Multiplicación:", result)

	//División
	result = y / x
	fmt.Println("División: ", result)

	//Modulo
	result = y % x
	fmt.Println("Modulo:", result)

	//Incremental
	x++
	fmt.Println("Incremental:", x)

	//Decremental
	x--
	fmt.Println("Decremental:", x)

	// Retos
	// Encontrar el area de un Rectangulo, trapecio y circulo.

	// Rectangulo
	const base int = 15
	const altura int = 10

	areaRectangular := base * altura

	fmt.Println("base:", base, "altura:", altura, "Area de un rectangulo:", areaRectangular)

	// Trapecio
	const longitud1 int = 8
	const longitud2 int = 12
	const alturaTrapecio int = 10

	areaTrapecio := (longitud1 + longitud2) / 2 * alturaTrapecio
	fmt.Println("Area de un trapecio: ", areaTrapecio)

	// Circulo
	const radio float64 = 3
	const pi float64 = 3.14

	areaCirculo := (radio * radio) * pi
	fmt.Println("Area de un circulo: ", areaCirculo)
}
